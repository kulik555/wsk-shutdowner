﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WskShutdowner
{
    class BasicTimeCounter : ITimeCounter
    {
        public BasicTimeCounter(Timer timer)
        {
            this.timer = timer;
        }

        public bool startTimer(int hour, int minutes)
        {
            if (hour == 0 && minutes == 0)
            {
                throw new System.Exception("Inserted null time values");
            }

            elapsedTime = new TimeSpan(hour, minutes, 0);
            timer.Interval = 1000;
            timer.Start();
            running = true;

            return running;
        }

        public bool isRunning()
        {
            return running;
        }

        public void stopTimer()
        {
            timer.Stop();
            running = false;
        }

        public void decrement()
        {
           elapsedTime = elapsedTime.Subtract(new TimeSpan(0, 0, 1));
        }

        public TimeSpan getElapsedTime()
        {
             return elapsedTime;
        }
        
        public Timer timer;
        TimeSpan elapsedTime;
        bool running;
    }
}
