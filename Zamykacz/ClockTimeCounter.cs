﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WskShutdowner
{
    class ClockTimeCounter : ITimeCounter
    {
        public ClockTimeCounter(Timer timer)
        {
            this.timer = timer;
        }
        public bool startTimer(int hour, int minutes)
        {
            DateTime currentTime = DateTime.Now;
            DateTime stopTime = DateTime.Parse(hour.ToString() + ":" + minutes.ToString());
            elapsedTime = calculateTimeDuration(currentTime, stopTime);

            timer.Interval = 1000;
            timer.Start();

            running = true;
            return running;
        }

        private TimeSpan calculateTimeDuration(DateTime currentTime, DateTime stopTime)
        {
            TimeSpan duration;

            if (stopTime < currentTime)
            {
                duration = new TimeSpan((24 - currentTime.Hour) + stopTime.Hour, 0, 0);
                duration = duration.Add(new TimeSpan(0, Math.Abs(currentTime.Minute - stopTime.Minute), 0));
            }
            else
            {
                duration = stopTime - currentTime;
            }
            return duration;
        }

        public bool isRunning()
        {
            return running;
        }

        public void stopTimer()
        {
            timer.Stop();
            running = false;
        }

        public void decrement()
        {
            elapsedTime.Subtract(new TimeSpan(0, 0, 1));
        }

        public TimeSpan getElapsedTime()
        {
            return elapsedTime;
        }

        public Timer timer;
        TimeSpan elapsedTime;
        bool running;
    }
}
