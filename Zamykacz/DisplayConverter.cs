﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WskShutdowner
{
    class DisplayConverter
    {
        public static string covertMinutesToString(int p_minutes)
        {
              return (p_minutes < 10) ? "0" + p_minutes.ToString()  : p_minutes.ToString();
        }

        public static string covertHoursToString(int p_hours)
        {
            return (p_hours < 10) ? "0" + p_hours.ToString() : p_hours.ToString();
        }
    }
}
