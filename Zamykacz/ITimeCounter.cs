﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WskShutdowner
{
    interface ITimeCounter
    {
         bool startTimer(int hour, int minutes);
         void stopTimer();
         bool isRunning();
         void decrement();
         TimeSpan getElapsedTime();
    }
}
