﻿namespace WskShutdowner
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.HourBox = new System.Windows.Forms.TextBox();
            this.MinutesBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.StartCountingButton = new System.Windows.Forms.Button();
            this.AbortButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.ChangeModeButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.StatusBarText = new System.Windows.Forms.TextBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // HourBox
            // 
            this.HourBox.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.HourBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.HourBox.Font = new System.Drawing.Font("Arial", 80.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HourBox.ForeColor = System.Drawing.Color.White;
            this.HourBox.Location = new System.Drawing.Point(21, 19);
            this.HourBox.MaxLength = 2;
            this.HourBox.Multiline = true;
            this.HourBox.Name = "HourBox";
            this.HourBox.Size = new System.Drawing.Size(120, 120);
            this.HourBox.TabIndex = 5;
            this.HourBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.HourBox.Click += new System.EventHandler(this.textBox1_Click);
            this.HourBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // MinutesBox
            // 
            this.MinutesBox.BackColor = System.Drawing.Color.DarkTurquoise;
            this.MinutesBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MinutesBox.Font = new System.Drawing.Font("Arial", 80.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MinutesBox.ForeColor = System.Drawing.Color.White;
            this.MinutesBox.Location = new System.Drawing.Point(161, 19);
            this.MinutesBox.MaxLength = 2;
            this.MinutesBox.Multiline = true;
            this.MinutesBox.Name = "MinutesBox";
            this.MinutesBox.Size = new System.Drawing.Size(120, 120);
            this.MinutesBox.TabIndex = 1;
            this.MinutesBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.MinutesBox.Click += new System.EventHandler(this.textBox2_Click);
            this.MinutesBox.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(496, 459);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Godz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(539, 434);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Min";
            // 
            // StartCountingButton
            // 
            this.StartCountingButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("StartCountingButton.BackgroundImage")));
            this.StartCountingButton.FlatAppearance.BorderSize = 0;
            this.StartCountingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartCountingButton.Location = new System.Drawing.Point(21, 159);
            this.StartCountingButton.Name = "StartCountingButton";
            this.StartCountingButton.Size = new System.Drawing.Size(260, 120);
            this.StartCountingButton.TabIndex = 4;
            this.StartCountingButton.UseVisualStyleBackColor = true;
            this.StartCountingButton.Click += new System.EventHandler(this.startCounting_Click);
            // 
            // AbortButton
            // 
            this.AbortButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("AbortButton.BackgroundImage")));
            this.AbortButton.FlatAppearance.BorderSize = 0;
            this.AbortButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AbortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AbortButton.ForeColor = System.Drawing.Color.White;
            this.AbortButton.Location = new System.Drawing.Point(304, 159);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(120, 120);
            this.AbortButton.TabIndex = 0;
            this.AbortButton.Text = "Anuluj ";
            this.AbortButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.abort_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(21, 314);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(132, 39);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(181, 469);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(129, 13);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "konrad.kulik1@gmail.com";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // ChangeModeButton
            // 
            this.ChangeModeButton.FlatAppearance.BorderSize = 0;
            this.ChangeModeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeModeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ChangeModeButton.ForeColor = System.Drawing.Color.White;
            this.ChangeModeButton.Image = ((System.Drawing.Image)(resources.GetObject("ChangeModeButton.Image")));
            this.ChangeModeButton.Location = new System.Drawing.Point(304, 19);
            this.ChangeModeButton.Name = "ChangeModeButton";
            this.ChangeModeButton.Size = new System.Drawing.Size(120, 120);
            this.ChangeModeButton.TabIndex = 6;
            this.ChangeModeButton.Text = "Odliczanie";
            this.ChangeModeButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ChangeModeButton.UseVisualStyleBackColor = true;
            this.ChangeModeButton.Click += new System.EventHandler(this.changeMode_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // StatusBarText
            // 
            this.StatusBarText.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.StatusBarText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StatusBarText.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.StatusBarText.ForeColor = System.Drawing.Color.White;
            this.StatusBarText.Location = new System.Drawing.Point(-4, 376);
            this.StatusBarText.Multiline = true;
            this.StatusBarText.Name = "StatusBarText";
            this.StatusBarText.ReadOnly = true;
            this.StatusBarText.Size = new System.Drawing.Size(444, 20);
            this.StatusBarText.TabIndex = 8;
            this.StatusBarText.Text = "Gotowy";
            this.StatusBarText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(35)))), ((int)(((byte)(44)))));
            this.ClientSize = new System.Drawing.Size(440, 395);
            this.Controls.Add(this.StatusBarText);
            this.Controls.Add(this.ChangeModeButton);
            this.Controls.Add(this.MinutesBox);
            this.Controls.Add(this.HourBox);
            this.Controls.Add(this.StartCountingButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.AbortButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "WSK";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyDown);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox HourBox;
        private System.Windows.Forms.TextBox MinutesBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button StartCountingButton;
        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button ChangeModeButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox StatusBarText;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

