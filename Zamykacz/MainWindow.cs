﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Timers;
using System.Management;


namespace WskShutdowner
{
    enum CurrentMode
    {
        counting,
        clock
    }

    public partial class MainWindow : Form
    {
        CurrentMode state;
        DisplayConverter dispConverter;
        ITimeCounter timeCounter;
        Shutdowner shutdowner;
        TimeCounterFactory counterFactory;

        public MainWindow()
        {
            InitializeComponent();
            HourBox.Text = "00";
            MinutesBox.Text = "00";
            notifyIcon1.Text = "WSK";
            notifyIcon1.Visible = true;
            this.ActiveControl = HourBox;
            this.KeyPreview = true;
            timeCounter = new BasicTimeCounter(timer1);
            shutdowner = new Shutdowner();
            counterFactory = new TimeCounterFactory(timer1);
        }

        private void unlockButtons()
        {
            HourBox.ReadOnly = false;
            MinutesBox.ReadOnly = false;
            StartCountingButton.Enabled = true;
        }

        private void lockButtons()
        {
            HourBox.ReadOnly = true;
            MinutesBox.ReadOnly = true;
            StartCountingButton.Enabled = false;
        }

        private void displayWorkingStatus()
        {
            StatusBarText.BackColor = System.Drawing.Color.Crimson;
            StatusBarText.Text = "Pracuję";
        }

        private void displayReadyStatus()
        {
            StatusBarText.BackColor = System.Drawing.Color.MediumSeaGreen;
            StatusBarText.Text = "Gotowy";
        }

        private void displayErrorStatus()
        {
            StatusBarText.BackColor = System.Drawing.Color.Gold;
            StatusBarText.Text = "Błąd";
        }

        private void resetDisplayedTime()
        {
            HourBox.Text = "00";
            MinutesBox.Text = "00";
        }

        private void startCounting_Click(object sender, EventArgs e)
        {
            int hour = 0;
            int min = 0;

            try
            {
                hour = Convert.ToInt32(HourBox.Text);
                min = Convert.ToInt32(MinutesBox.Text);
                timeCounter = counterFactory.createCounter(state);
                timeCounter.startTimer(hour, min);
            }
            catch
            {
                displayErrorStatus();
                return;
            }

            if (timeCounter.isRunning())
            {
                lockButtons();
                displayWorkingStatus();
                state = CurrentMode.counting;
                ChangeModeButton.Text = "Odliczanie";
            }

            ChangeModeButton.Select();
        }

        private void abort_Click(object sender, EventArgs e)
        {
            if (timeCounter.isRunning())
            {
                shutdowner.abort();
                resetDisplayedTime();
                unlockButtons();
                displayReadyStatus();

                timeCounter.stopTimer();
                timeCounter = new BasicTimeCounter(timer1);

                notifyIcon1.BalloonTipText = "Anulowano odliczanie";
                notifyIcon1.ShowBalloonTip(3000);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            HourBox.SelectAll();
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            MinutesBox.SelectAll();
        }

        private void changeMode_Click(object sender, EventArgs e)
        {
            if (!timeCounter.isRunning())
            {
                switch (state)
                {
                    case CurrentMode.counting:

                        state = CurrentMode.clock;
                        ChangeModeButton.Text = "Godzina";
                        HourBox.Text = DisplayConverter.covertHoursToString(DateTime.Now.Hour);
                        MinutesBox.Text = DisplayConverter.covertMinutesToString(DateTime.Now.Minute);
                        break;

                    case CurrentMode.clock:

                        state = CurrentMode.counting;
                        ChangeModeButton.Text = "Odliczanie";
                        resetDisplayedTime();
                        break;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timeCounter.decrement();

            TimeSpan elapsedTime = timeCounter.getElapsedTime();
            HourBox.Text = DisplayConverter.covertHoursToString(elapsedTime.Hours);
            MinutesBox.Text = DisplayConverter.covertMinutesToString(elapsedTime.Minutes);

            if (elapsedTime.TotalSeconds == 0 && !shutdowner.isShutdownPerformed())
            {
                shutdowner.performShutdown();
            }
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (timeCounter.isRunning())
            {
                timeCounter.stopTimer();
            }
        }

        private void MainWindow_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                TimeSpan elapsedTime = timeCounter.getElapsedTime();
                HourBox.Text = DisplayConverter.covertHoursToString(elapsedTime.Hours);
                MinutesBox.Text = DisplayConverter.covertMinutesToString(elapsedTime.Minutes);

                notifyIcon1.Visible = true;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.ActiveControl = MinutesBox;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (HourBox.Text.Length >= 2)
                this.ActiveControl = MinutesBox;
        }

        private void MainWindow_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !timeCounter.isRunning())
            {
                startCounting_Click(this, e);
                this.ActiveControl = StartCountingButton;
            }

            if (e.KeyCode == Keys.Escape && timeCounter.isRunning())
            {
                abort_Click(this, e);
                this.ActiveControl = HourBox;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (MinutesBox.Text.Length >= 2)
                this.ActiveControl = HourBox;
        }
    }
}
