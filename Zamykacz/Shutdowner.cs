﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;

namespace WskShutdowner
{
    class Shutdowner
    {
        public Shutdowner()
        {
            win32 = new ManagementClass("Win32_OperatingSystem");
            win32.Get();
            win32.Scope.Options.EnablePrivileges = true;

            shutdownParams = win32.GetMethodParameters("Win32Shutdown");
            shutdownParams["Flags"] = "5";
            shutdownParams["Reserved"] = "0";
            shutdownPerformed = false;
        }

        public void performShutdown()
        {
            foreach (ManagementObject manObj in win32.GetInstances())
            {
                shutdownObject = manObj.InvokeMethod("Win32Shutdown", shutdownParams, null);
            }
            shutdownPerformed = true;
        }

        public void abort()
        {
            if (shutdownPerformed)
            {
                Process.Start("shutdown", "/a");
            }
        }

        public bool isShutdownPerformed()
        {
            return shutdownPerformed;
        }

        private ManagementBaseObject shutdownObject;
        private ManagementClass win32;
        private ManagementBaseObject shutdownParams;
        private bool shutdownPerformed;
    }
}
