﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WskShutdowner
{
    class TimeCounterFactory
    {
        public TimeCounterFactory(Timer timer)
        {
            this.timer = timer;
        }
         public ITimeCounter createCounter(CurrentMode state)
        {
            if (state == CurrentMode.counting)
            {
                return new BasicTimeCounter(timer);
            }
            else
            {
                return new ClockTimeCounter(timer);
            }
        }

        private Timer timer;
    }
}
